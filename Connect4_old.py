import itertools
import copy

class Connect4:
    def __init__(self, num_of_players=2):
        self.game_running = True
        self.num_of_players=num_of_players
        while True:
            self.grid=self.get_grid_settings()
            if self.grid ==False:
                continue
            else:
                break
        
        
    def get_user_num(self, text, min_, max_):
        '''Generalises getting a number in a range (inclusive). returns the number if valid, and None if they exit.'''
        while True:
            user_input=input(text+"\t").lower()
            if user_input=="exit" or user_input=="end":
                return None
            try:
                user_int=int(user_input)
                if user_int<min_ or user_int>max_:
                    print(f"Please choose a number between {min_} and {max_}.")
                else:
                    return user_int
            except ValueError: #haven't entered an int
                print("That doesn't look right. Please enter a whole number in digits, or type exit to end the program.")
            
    def get_user_yes_no(self, text):
        '''Generalises getting a yes or no answer from the user. Returns True for yes, False for no, or None if they exit.'''
        while True:
            user_input=input(text+"(Y/N)\t").lower()
            if user_input=="exit" or user_input=="end":
                return None
            if user_input=="y" or user_input=="yes":
                return True
            if user_input=="n" or user_input=="no":
                return False
            else:
                print("That doesn't look right. Please enter y, n, or type exit to end the program.")
                        
        

    def get_grid_settings(self):
        '''Method to parse user preferences regarding the game board. Returns a Grid if successful, False otherwise.'''
        
        regular_game=self.get_user_yes_no("Play a regular game?")
        if regular_game==None:
            return False
        elif regular_game:
            return RectGrid(7,6) #standard game      
        elif not regular_game:
            
            print("Advanced Settings: ")
            num_cols=self.get_user_num("How many columns would you like?", 1, 25)
            if num_cols==None:
                return False
            else:
                fixed_height=self.get_user_yes_no("Would you like every column to have the same number of rows?")
                if fixed_height==None:
                    return False
                elif fixed_height:
                    height=self.get_user_num("How many rows should that be?", 1, 25)
                    return RectGrid(num_cols,height) #they have specified columns and rows
                elif not fixed_height:
                    heights=[]
                    for c in range(num_cols):#ask once for each column
                        c_height=self.get_user_num(f"How many rows tall should column {c+1} be?", 1, 25)
                        if c_height==None:
                            return False
                        else:
                            heights.append(c_height)
                    return Grid(num_cols, heights)

        
    def get_user_choice(self, num_of_columns, player_num):
        '''Method to find out in which column the user wants to play.'''
        while True:
            user_input=input(f"Player {player_num}: choose a column to play:\n")
            if user_input.lower()=='exit':
                print("Thanks for playing :)")
                self.game_running=False
                return None
            try:
                column_chosen=int(user_input)
                if column_chosen>0 and column_chosen<=num_of_columns:
                    if self.grid.column_is_full(column_chosen-1):
                        print("That column is full. Please choose another.")
                        continue
                    else:
                        return column_chosen-1
                else:
                    print(f"Out of bounds: please choose a number between 1 and {num_of_columns}")
                    continue
            except ValueError:
                print("Please enter a whole number, or type \'exit\' to quit")
                
    def run_game(self):
        num_of_players=self.num_of_players
        turn_count=0
        while self.game_running:

            print(self.grid)#print the game grid
            
            player_num=(turn_count%num_of_players)+1
            chosen_column=self.get_user_choice(len(self.grid.columns), player_num)
            if not self.game_running: #kills the game if the user has told it to stop at input
                break
            
            self.grid.add_to_column(chosen_column, self.get_player_symbol(player_num))
            
            winner=self.grid.check_for_winner()
            if winner!=None:
                print(self.grid)#print the game grid
                if winner=="Draw":
                    print("\nThe game ended as a draw. Thanks for playing!")
                else:
                    print(f"\nPlayer {winner} wins the game! Congratulations :)")
                break #end the game
                
            turn_count+=1
            
    def get_player_symbol(self, player_number):
        symbol_list=["O", "X", "A", "B"]
        try:
            return symbol_list[player_number-1]
        except IndexError:
            return player_number
        
        
        

        
class Grid:
    '''Abstract Grid Class to store the game grid for Connect4. Number of columns is specified, but number of rows
       (i.e. maximimum column height) may vary over columns.'''
    def __init__(self, num_columns, max_heights):
        self.num_columns=num_columns
        self.columns=[[] for c in range(num_columns)] #each entry is a column, viewed as a list of some height
        self.max_column_heights=max_heights
        self.lines=self.gen_lines()
    
    def __str__(self):
        '''Method which is automatically called when you print a Grid object
        #Zip the columns together to get the nth row as the nth entry in each column.
        #Pad for height, with a new line, and then reverse the string so it's not upside down
        '''
        
        display_columns=copy.deepcopy(self.columns)#deep copy of columns, so it copies the lists inside too (instead of pointers)
        for c in range(self.num_columns): #pad each column up to its maximum height using underscores
            for r in range(len(display_columns[c]), self.max_column_heights[c]):
                display_columns[c].append('_')
                
        #contains a list of rows, from bottom to top
        output_rows=[str(" ".join(t)) for t in itertools.zip_longest(*display_columns, fillvalue=' ')]
        #reverse to get top to bottom, and then each on a new line.
        return("\n"+"\n".join(reversed(output_rows)))

        
    def add_to_column(self, column_num, value):
        if len(self.columns[column_num]) < self.max_column_heights[column_num]: #if the column specified is below its max allowed height
            self.columns[column_num].append(value)
            
    def check_for_winner(self):
        for l in self.gen_lines():
            tokens_in_line={self.columns[i][j] for (i,j) in l}
            if len(tokens_in_line)==1 and list(tokens_in_line)[0]!=None:
                return list(tokens_in_line)[0]
        
        for i in range(self.num_columns):
            if not self.column_is_full(i):
                return None #there is no winner, but the board is not full
        return "Draw" #the board is full but there is no winner
    
    def gen_lines(self):
        '''Returns a list of all lines (lists of 4 adjacent coordinate pairs) in the grid.'''
        line_list=[]
        valid_coords=[]
        for i in range(len(self.columns)):
            for j in range(len(self.columns[i])):
                valid_coords.append((i,j))
        valid_coords=set(valid_coords)
                
                

        for i in range(self.num_columns):
            for j in range(6):
                if {(i,j),(i,j+1),(i,j+2),(i,j+3)}.issubset(valid_coords):
                    line_list.append([(i,j),(i,j+1),(i,j+2),(i,j+3)])
                    
                if {(i,j),(i+1,j),(i+2,j),(i+3,j)}.issubset(valid_coords):    
                    line_list.append([(i,j),(i+1,j),(i+2,j),(i+3,j)])
                    
                if {(i,j),(i+1,j+1),(i+2,j+2),(i+3,j+3)}.issubset(valid_coords):
                    line_list.append([(i,j),(i+1,j+1),(i+2,j+2),(i+3,j+3)])
                    
                if {(i,j),(i-1,j-1),(i-2,j-2),(i-3,j-3)}.issubset(valid_coords):
                    line_list.append([(i,j),(i-1,j-1),(i-2,j-2),(i-3,j-3)])
            
        return line_list
    
    def column_is_full(self, column_num):
        return len(self.columns[column_num])==self.max_column_heights[column_num]
    
    def tuple_has_winner(self, line):
        '''Method which takes a tuple and returns whether they have a '''
        pass
        
        
class RectGrid(Grid):
    def __init__(self, columns, rows):
        super().__init__(columns, [rows for i in range(columns)])
       

c = Connect4()
c.run_game()
    
    