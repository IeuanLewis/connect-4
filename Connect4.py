import itertools
import os
import json
from random import randint


class Connect4:
    def __init__(self, num_of_players=2):
        self.game_running = True
        self.num_of_players=num_of_players
        self.grid=False
        self.save_file = 'connect_4_save.txt'
        self.leaderboard ={}
        
        
    def get_user_num(self, text, min_, max_):
        '''Generalises getting a number in a range (inclusive). returns the number if valid, and None if they exit.'''
        while True:
            user_input=input(text+"\t").lower()
            if user_input=="exit" or user_input=="end":
                return None
            if user_input=='m':
                return 'm'
            try:
                user_int=int(user_input)
                if user_int<min_ or user_int>max_:
                    print(f"Please choose a number between {min_} and {max_}.")
                else:
                    return user_int
            except ValueError: #haven't entered an int
                print("That doesn't look right. Please enter a whole number in digits, or type exit to end the program.")
            
    def get_user_yes_no(self, text):
        '''Generalises getting a yes or no answer from the user. Returns True for yes, False for no, or None if they exit.'''
        while True:
            user_input=input(text+"(Y/N)\t").lower()
            if user_input=="exit" or user_input=="end":
                return None
            if user_input=="y" or user_input=="yes":
                return True
            if user_input=="n" or user_input=="no":
                return False
            else:
                print("That doesn't look right. Please enter y, n, or type exit to end the program.")
                        
        

    def get_grid_settings(self):
        '''Method to parse user preferences regarding the game board. Returns a Grid if successful, False otherwise.'''
        
        regular_game=self.get_user_yes_no("Play a regular game?")
        if regular_game==None:
            return False
        elif regular_game:
            return RectGrid(7,6) #standard game      
        elif not regular_game:
            
            print("Advanced Settings: ")
            num_cols=self.get_user_num("How many columns would you like?", 1, 25)
            if num_cols==None:
                return False
            else:
                fixed_height=self.get_user_yes_no("Would you like every column to have the same number of rows?")
                if fixed_height==None:
                    return False
                elif fixed_height:
                    height=self.get_user_num("How many rows should that be?", 1, 25)
                    return RectGrid(num_cols,height) #they have specified columns and rows
                elif not fixed_height:
                    heights=[]
                    for c in range(num_cols):#ask once for each column
                        c_height=self.get_user_num(f"How many rows tall should column {c+1} be?", 1, 25)
                        if c_height==None:
                            return False
                        else:
                            heights.append(c_height)
                    return Grid(num_cols, heights)
                
    def get_user_menu_option(self):
        log = ''
        while True:
            self.clear()
            print('-----------------MENU-----------------')
            print(log)
            print('enter s to save current game')
            print('enter l to load previous game')
            print('enter e to exit menu')
            print('enter end to end the game')
            menu_in = input('... ')
            
            if menu_in == 's':
                log = self.save_game()
            elif menu_in == 'l':
                log = self.load_game()
            elif menu_in == 'e':
                return
            elif menu_in == 'end':
                print("Thanks for playing :)")
                self.game_running==False
                return None

    def clear(self):
        clear_screen = lambda: os.system('cls')
        clear_screen()
        return None
        
        
    def run_game(self):
        self.computer_active =  self.get_user_yes_no(r'Would you like to play against a computer?')
        self.grid=self.get_grid_settings()
        if self.grid==False: #if they exit the options
            return
        
        self.get_user_names()
        
        num_of_players=self.num_of_players
        turn_count=0
        
        
        while self.game_running:

            self.clear()

            print(self.grid)#print the game grid
            
            player_num=(turn_count%num_of_players)+1
            self.user_name = self.user_names[player_num-1]
            get_user_col_text = f"{self.user_name} ({self.get_player_symbol(player_num)}): choose a column to play:"
            
            while True:
                
                if self.computer_active and self.get_player_symbol(player_num) == 'O':
                    chosen_column = self.grid.computer_play() + 1
                else:
                    chosen_column=self.get_user_num(get_user_col_text, 1, len(self.grid.columns))
                
                
                if chosen_column==None:
                    print("Thanks for playing :)")
                    self.game_running==False
                    return
                if chosen_column == 'm':
                    log = ''
                    while True:
                        self.clear()
                        print('-----------------MENU-----------------')
                        print(log)
                        print('enter s to save current game')
                        print('enter l to load previous game')
                        print('enter e to exit menu')
                        print('enter end to end the game')
                        menu_in = input('... ')
                        
                        if menu_in == 's':
                            log = self.save_game()
                        elif menu_in == 'l':
                            log = self.load_game()
                        elif menu_in == 'e':
                            chosen_column=self.get_user_num(get_user_col_text, 1, len(self.grid.columns)) 
                            break
                        elif menu_in == 'end':
                            print("Thanks for playing :)")
                            self.game_running==False
                            return
                        
                if self.grid.add_to_column(chosen_column-1, self.get_player_symbol(player_num)):
                    break
                else:
                    print("That column is already full!\n")
            
            self.winner=self.grid.check_for_winner()
            if self.winner!=None:
                print(self.grid)#print the game grid
                if self.winner=="Draw":
                    print("\nThe game ended as a draw. Thanks for playing!")
                    self.update_leaderboard()
                else:
                    print(f"\nPlayer ({self.winner}) wins the game! Congratulations :)")
                    self.update_leaderboard()
                break #end the game
                
            turn_count+=1
            
    def get_player_symbol(self, player_number):
        self.symbol_list=["O", "X", "A", "B"]
        try:
            return self.symbol_list[player_number-1]
        except IndexError:
            return player_number
        
    def save_game(self):
        
        log = 'game saved successfully'
        s_list = []
        s_list.append(self.num_of_players)
        s_list.append(self.grid.num_columns)
        s_list.append(self.grid.columns)
        s_list.append(self.grid.max_column_heights)
        s_list.append(self.grid.lines)
        
        with open(self.save_file,'w') as f:
            f.write(json.dumps(s_list))
        
        
        return log
        
    def load_game(self):
        log = 'game loaded successfully'
        
        s_list = []
        with open(self.save_file, 'r') as f:
            s_list = json.loads(f.read())
        self.num_of_players = s_list[0]
        self.grid.num_columns = s_list[1]
        self.grid.columns = s_list[2]
        self.grid.max_column_heights = s_list[3]
        self.grid.lines = s_list[4]
        return log        
        
    def get_user_names(self):
        self.user_names=[]
        for i in range(self.num_of_players):
            name_input_text = f'Player {i+1} ({self.get_player_symbol(i+1)}), what is your name?'
            if self.computer_active and i == 0:
                name= 'computer'
            else:
                name= input(name_input_text)
            while name in self.user_names:
                print('Another player has already taken this name. Please use a different name')
                name= input(name_input_text)
            self.user_names.append(name)
            if name not in self.leaderboard:
                self.leaderboard[name]={'Wins':0, 'Draws':0, 'Loses':0}
        
    def update_leaderboard(self):
        if self.winner=='Draw':
            for i in range(self.num_of_players):
                self.leaderboard[self.user_names[i-1]]['Draws'] += 1             
        elif self.winner!=None:
             for i in range(self.num_of_players): 
                if self.user_name == self.user_names[i-1]: 
                    self.leaderboard[self.user_names[i-1]]['Wins'] += 1
                else:
                    self.leaderboard[self.user_names[i-1]]['Loses'] += 1           
        # Could add feature to save 'leaderboard in game file'
        for name, results in self.leaderboard.items():
            print(f"{name} -- Wins:{results['Wins']}  Draws:{results['Draws']}  Loses:{results['Loses']}")
            


                
                
class Grid:
    '''Abstract Grid Class to store the game grid for Connect4. Number of columns is specified, but number of rows
       (i.e. maximimum column height) may vary over columns.'''
    def __init__(self, num_columns, max_heights):
        self.num_columns=num_columns
        self.columns=[[] for c in range(num_columns)] #each entry is a column, viewed as a list of some height
        self.max_column_heights=max_heights
        self.lines=self.gen_lines()
    
    def __str__(self):
        '''
        Zip the columns together to get the nth row as the nth entry in each column.
        Pad for height, with a new line, and then reverse the string so it's not upside down
        '''
        display_columns=[c.copy() for c in self.columns] #deep copy of columns, so it copies the lists inside too (instead of pointers)
        for c in range(self.num_columns): #pad each column up to its maximum height using underscores
            for r in range(len(display_columns[c]), self.max_column_heights[c]):
                display_columns[c].append('_')
                
        #contains a list of rows, from bottom to top
        output_rows=["\t"+str(" ".join(t)) for t in itertools.zip_longest(*display_columns, fillvalue=' ')]
        #reverse to get top to bottom, and then each on a new line.
        return("\n"+"\n".join(reversed(output_rows)))

        
    def add_to_column(self, column_num, value):
        if len(self.columns[column_num]) < self.max_column_heights[column_num]: #if the column specified is below its max allowed height
            self.columns[column_num].append(value)
            return True
        else:
            return False
            
    def check_for_winner(self):
        for l in self.gen_lines():
            tokens_in_line={self.columns[i][j] for (i,j) in l}
            if len(tokens_in_line)==1 and list(tokens_in_line)[0]!=None:
                return list(tokens_in_line)[0]
        
        for i in range(self.num_columns):
            if not self.column_is_full(i):
                return None #there is no winner, but the board is not full
        return "Draw" #the board is full but there is no winner
    
    def gen_lines(self):
        '''Returns a list of all lines (lists of 4 adjacent coordinate pairs) in the grid.'''
        line_list=[]
        valid_coords=[]
        for i in range(len(self.columns)):
            for j in range(len(self.columns[i])):
                valid_coords.append((i,j))
        valid_coords=set(valid_coords)
                
                

        for i in range(self.num_columns):
            for j in range(6):
                if {(i,j),(i,j+1),(i,j+2),(i,j+3)}.issubset(valid_coords):
                    line_list.append([(i,j),(i,j+1),(i,j+2),(i,j+3)])
                    
                if {(i,j),(i+1,j),(i+2,j),(i+3,j)}.issubset(valid_coords):    
                    line_list.append([(i,j),(i+1,j),(i+2,j),(i+3,j)])
                    
                if {(i,j),(i+1,j+1),(i+2,j+2),(i+3,j+3)}.issubset(valid_coords):
                    line_list.append([(i,j),(i+1,j+1),(i+2,j+2),(i+3,j+3)])
                    
                if {(i,j),(i+1,j-1),(i+2,j-2),(i+3,j-3)}.issubset(valid_coords):
                    line_list.append([(i,j),(i+1,j-1),(i+2,j-2),(i+3,j-3)])
            
        return line_list
    
    def column_is_full(self, column_num):
        return len(self.columns[column_num])==self.max_column_heights[column_num]
    

                
                
    def computer_play(self):
        valid_coords=[]
        for i in range(len(self.columns)):
            for j in range(len(self.columns[i])):
                valid_coords.append((i,j))
        valid_coords=set(valid_coords)
                

                
        optimal_choice = self.find_optimums(3, valid_coords, 'O')
        if len(optimal_choice) == 0:
            optimal_choice = self.find_optimums(3, valid_coords, 'X')
        if len(optimal_choice) == 0:
            optimal_choice = self.find_optimums(2, valid_coords, 'O')
        if len(optimal_choice) == 0:

            optimal_choice = self.find_optimums(1, valid_coords, 'O')
        if len(optimal_choice) == 0:
            optimal_choice = {(1,1)}
            
            
        for item in optimal_choice:
            if item[1]  < self.max_column_heights[item[0]] and item[0] <= len(self.columns) - 1 and item[1] == len(self.columns[item[0]]):
                return item[0]
        random_choice = randint(0, len(self.columns)-1)
        if self.max_column_heights[random_choice] - len(self.columns[random_choice]) < 4:
            random_choice = randint(0, len(self.columns)-1)
        return random_choice
                
                
    def find_optimums(self, number_in_a_row, valid_coords, player):
        line_list=[]
        optimal_choice = list()
        
        for i in range(self.num_columns):
            for j in range(len(self.columns[i])):
                possible_combinations1 = set([])
                possible_combinations2 = set([])
                possible_combinations3 = set([])
                possible_combinations4 = set([])
                x = number_in_a_row
                while x > 0:
                    possible_combinations1.add((i,j + (number_in_a_row - x)))
                    if possible_combinations1.issubset(valid_coords):
                        line_list.append(list(sorted(possible_combinations1)))

                        
                    possible_combinations2.add((i + (number_in_a_row - x),j))
                    if possible_combinations2.issubset(valid_coords):
                        line_list.append(list(possible_combinations2))

                    possible_combinations3.add((i + (number_in_a_row - x),j + (number_in_a_row - x)))
                    if possible_combinations3.issubset(valid_coords):
                        line_list.append(list(possible_combinations3))

                    possible_combinations4.add((i - (number_in_a_row - x),j - (number_in_a_row - x)))
                    if possible_combinations4.issubset(valid_coords):
                        line_list.append(list(possible_combinations4))
                        
                    # possible_combinations4.add((i + (number_in_a_row - x),j - (number_in_a_row - x)))
                    # if possible_combinations4.issubset(valid_coords):
                    #     line_list.append(list(possible_combinations4))
                        
                    # possible_combinations4.add((i - (number_in_a_row - x),j + (number_in_a_row - x)))
                    # if possible_combinations4.issubset(valid_coords):
                    #     line_list.append(list(possible_combinations4))
                        
                
                        
                    x -= 1
                         
        for l in line_list:
            tokens_in_line = {self.columns[i][j] for (i,j) in l}
            l = sorted(l, key = lambda tup: tup[1])
            
            if len(tokens_in_line)==1 and list(tokens_in_line)[0]!=None and list(tokens_in_line)[0] == player and len(l) == number_in_a_row:

                if l[0][0] != l[-1][0] and l[0][1] != l[-1][1] and (l[-1][0] + 1) < len(self.columns) - 1 and (l[-1][0] - 1) >= 0:
                    if len(self.columns[(l[-1][0] + 1)]) == (l[-1][1] + 1):
                                        optimal_choice.append((l[-1][0] + 1, l[-1][1] + 1))
                    if len(self.columns[(l[0][0] - 1)]) == (l[0][1] - 1):
                                        optimal_choice.append((l[0][0] - 1, l[0][1] - 1))

                
                elif l[0][0] == l[-1][0] and l[0][1] != l[-1][1]:
                    if len(self.columns[(l[-1][0])]) == (l[-1][1] + 1):
                                        optimal_choice.append((l[-1][0], l[-1][1] + 1))

                    
                elif l[0][0] != l[-1][0] and l[0][1] == l[-1][1] and (l[-1][0] + 1) < len(self.columns) - 1 and (l[-1][0] - 1) >= 0:
                    l = sorted(l, key = lambda tup: tup[0])
                    if len(self.columns[(l[-1][0] + 1)]) == (l[-1][1]):
                        optimal_choice.append((l[-1][0] + 1, l[-1][1]))
                    if len(self.columns[(l[0][0] - 1)]) == (l[0][1]):
                        optimal_choice.append((l[0][0] - 1, l[0][1]))

        return set(optimal_choice)


        
class RectGrid(Grid):
    def __init__(self, columns, rows):
        super().__init__(columns, [rows for i in range(columns)])
       

c = Connect4()

c.run_game()
print('second update to check')
c.run_game()